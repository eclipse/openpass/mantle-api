This document serves as a foundational reference for all contributors.
Any modifications to this document or related files (such as .clang-format) require approval from the primary contributors.
To propose changes, please submit an issue along with a corresponding merge request.

[TOC]

# Foundation

- Use C++17
- Do not use special compiler extensions (e.g. specific gcc attributes)

# Code Organization

- Put mantle related definitions into the namespace `mantle_api`.

# Documentation

- Use doxygen for documentation
- Use tripple slash notation `/// Docs look like this`
- Don't use brief (automatic by default)
- Prefere imperative style for descriptions (`/// Set Position` - not `/// Sets the Position`)
- Document everything, i.e. also private members and indiviudal values of an enumeration.
- Use inline style for members and values (`int my_value ///< goes here`)

# Testing

- We use googletest
- Provide a mock for each interface
- Each interface (mock) must be referenced in a test

# 🦾 CI Enforced Conventions

We don't use git hooks, but our CI checks our rules.

## C++

 - **Compiler Flags**: We treat warnings as errors. For the rest see [`CMakeLists.txt`](CMakeLists.txt)
 - **Static Code Analysis**:  
   - We use [`.clang-tidy`](.clang-tidy) and don't accept any warnings/errors
   - ⚠️ There are adjusted rules for testing [`test/.clang-tidy`](test/.clang-tidy)
- **Naming**: Also covered through [`.clang-tidy`](.clang-tidy) → Rule `readability-identifier-naming`
- **Formatting**: [`.clang-format`](.clang-format) → It's best, when you format on safe using the same rules.
- **Documentation**: We use a generated `Doxyfile` (see [`doc/CmakeLists.txt`](doc/CmakeLists.txt))

## CMake

- **Formatting**: [`.cmake-format`](.cmake-format)
