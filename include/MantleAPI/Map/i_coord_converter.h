/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file i_coord_converter.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_I_COORD_CONVERTER_H
#define MANTLEAPI_MAP_I_COORD_CONVERTER_H

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

namespace mantle_api
{

/// Interface that provides functionality to query the underlying map with regard to transformation of different
/// position types, curvature, elevation etc.
class ICoordConverter
{
public:
  virtual ~ICoordConverter() = default;

  /// Converts a position of any kind to its corresponding inertial position.
  ///
  /// @param position The input position. Coordinate system is implicitly defined by underlying type of Position
  /// @return Inertial position
  [[nodiscard]] virtual Vec3<units::length::meter_t> Convert(Position position) = 0;

  /// Calculates the orientation of the lane center line at a given lane position.
  ///
  /// @param open_drive_lane_position Position specified in a lane coordinate system (road id, local lane id, s)
  /// @return Orientation of the lane center line at the specified position in inertial coordinates
  [[nodiscard]] virtual Orientation3<units::angle::radian_t> GetLaneOrientation(
      const OpenDriveLanePosition& open_drive_lane_position) = 0;

  /// Calculates the orientation of the road reference line at a given road position.
  ///
  /// @param open_drive_road_position Position specified in a road coordinate system (road id, s)
  /// @return Orientation of the road reference line at the specified position in inertial coordinates
  [[nodiscard]] virtual Orientation3<units::angle::radian_t> GetRoadOrientation(
      const OpenDriveRoadPosition& open_drive_road_position) = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_MAP_I_COORD_CONVERTER_H
