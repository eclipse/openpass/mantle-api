/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 * Copyright (c) 2024, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file i_scenario_engine.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_EXECUTION_I_SCENARIO_ENGINE_H
#define MANTLEAPI_EXECUTION_I_SCENARIO_ENGINE_H

#include <MantleAPI/Execution/i_steppable.h>
#include <MantleAPI/Execution/scenario_info.h>

namespace mantle_api
{

/// Base interface for the scenario.
class IScenarioEngine : public ISteppable
{
public:
  virtual ~IScenarioEngine() = default;

  /// Parse and validate the scenario and referenced catalogs.
  /// @note A map file referenced from the scenario is not parsed.
  ///       The map file path can be retrieved after calling this method with `GetScenarioInfo()`.
  /// @throw If the scenario or catalogs contain errors.
  virtual void Init() = 0;

  /// Provide information about the scenario parsed in `Init()`.
  /// @return information about the scenario
  [[nodiscard]] virtual ScenarioInfo GetScenarioInfo() const = 0;

  /// Reset and set up all dynamic content, for example create entities and controllers.
  /// Can be called after Init().
  /// Can be called multiple times for consecutive simulation runs.
  virtual void SetupDynamicContent() = 0;

  /// Indicate whether the scenario implementation has finished processing the scenario (end of scenario is reached).
  /// @return `true` if processing the scenario is complete, `false` otherwise.
  [[nodiscard]] virtual bool IsFinished() const = 0;

  /// Activate external controller for the host.
  virtual void ActivateExternalHostControl() = 0;

  /// Parse and validate the scenario and referenced catalogs.
  /// @return 0 on success, >0 = Number of scenario errors
  virtual int ValidateScenario() = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_EXECUTION_I_SCENARIO_ENGINE_H
