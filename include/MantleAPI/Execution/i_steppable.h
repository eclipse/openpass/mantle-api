/*******************************************************************************
 * Copyright (c) 2024, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file i_steppable.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_EXECUTION_I_STEPPABLE_H
#define MANTLEAPI_EXECUTION_I_STEPPABLE_H

#include <MantleAPI/Common/time_utils.h>

#include <optional>

/// MantleAPI namespace
namespace mantle_api
{

/// Interface for classes whose state can be updated based on the elapsed time.
struct ISteppable
{
  virtual ~ISteppable() = default;

  /// Update the state of the class based on the elapsed time.
  /// @param[in] delta_time The elapsed time since the last step
  virtual void Step(Time delta_time) = 0;

  /// Get the desired delta time for the next step.
  /// @return The desired delta time for the next step, or `std::nullopt` if the delta time is not fixed
  [[nodiscard]] virtual std::optional<Time> GetDesiredDeltaTime() const noexcept = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_EXECUTION_I_STEPPABLE_H
