/*******************************************************************************
 * Copyright (c) 2024, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file i_environment_engine.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_EXECUTION_I_ENVIRONMENT_ENGINE_H
#define MANTLEAPI_EXECUTION_I_ENVIRONMENT_ENGINE_H

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Execution/i_steppable.h>

/// MantleAPI namespace
namespace mantle_api
{

/// Interface for classes that manage the environment.
struct IEnvironmentEngine : public ISteppable
{
  virtual ~IEnvironmentEngine() = default;

  /// Get the environment.
  /// @return The environment
  virtual IEnvironment* GetEnvironment() noexcept = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_EXECUTION_I_ENVIRONMENT_ENGINE_H
