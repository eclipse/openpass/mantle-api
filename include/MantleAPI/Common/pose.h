/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file pose.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_POSE_H
#define MANTLEAPI_COMMON_POSE_H

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <iostream>

namespace mantle_api
{

/// Pose in the Cartesian coordinate system
struct Pose
{
  /// Position defined in terms of the x/y/z coordinates in the Cartesian coordinate system
  Vec3<units::length::meter_t> position{};
  /// Orientation defined in terms of the Yaw/Pitch/Roll orientation angles in the Cartesian coordinate system
  Orientation3<units::angle::radian_t> orientation{};

  /// @brief Equality comparison for Pose.
  ///
  /// @param[in]  other The left-hand side value for the comparison
  /// @returns  true if the values of `this` exactly equal to the values of other.
  bool operator==(const Pose& other) const
  {
    return other.position == position && other.orientation == orientation;
  }

  /// @brief Prints a human-readable representation of 'pose' to 'os'.
  /// @param os     The output stream
  /// @param pose   Open scenario pose
  /// @returns 'pose' in a human-readable format
  friend inline std::ostream& operator<<(std::ostream& os, const Pose& pose);
};

/// @brief Prints a human-readable representation of 'pose' to 'os'.
/// @param os     The output stream
/// @param pose   Open scenario pose
/// @returns 'pose' in a human-readable format
std::ostream& operator<<(std::ostream& os, const Pose& pose)
{
  os << "position ("
     << pose.position.x
     << ", " << pose.position.y
     << ", " << pose.position.z
     << "), orientation (" << pose.orientation.yaw
     << ", " << pose.orientation.pitch
     << ", " << pose.orientation.roll
     << ")";

  return os;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_POSE_H
