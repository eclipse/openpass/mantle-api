/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file clothoid_spline.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_CLOTHOID_SPLINE_H
#define MANTLEAPI_COMMON_CLOTHOID_SPLINE_H

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/unit_definitions.h>
#include <units.h>

#include <optional>
#include <tuple>
#include <variant>
#include <vector>

namespace mantle_api
{

/// This struct represents one segment of a clothoid spline
struct ClothoidSplineSegment
{
  units::curve::curvature_t curvature_end;    ///< End curvature of the clothoid segment. Unit: [1/m]. Range: ]-inf..inf[.
  units::curve::curvature_t curvature_start;  ///< Start curvature of the clothoid segment. Unit: [1/m]. Range: ]-inf..inf[.
  units::angle::radian_t h_offset;            ///< Heading offset relative to end of the previous segment or to position_start if present. Unit: [rad]. Range: ]-pi..pi[.
  units::length::meter_t length;              ///< Length of the clothoid segment. Unit: [m]. Range: ]0..inf[.
  std::optional<Pose> position_start;         ///< Optional start position of the clothoid segment.
};

/// Compare two objects of ClothoidSplineSegment.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]  rhs right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equals to rhs values
constexpr bool operator==(const ClothoidSplineSegment& lhs, const ClothoidSplineSegment& rhs) noexcept
{
  return std::tie(lhs.curvature_end, lhs.curvature_start, lhs.h_offset, lhs.length, lhs.position_start) ==
         std::tie(rhs.curvature_end, rhs.curvature_start, rhs.h_offset, rhs.length, rhs.position_start);
};

/// Print a human-readable representation of 'ClothoidSplineSegment' to 'os'.
///
/// @param os             The output stream
/// @param clothoid_spline_segment  ClothoidSplineSegment to be printed
/// @returns 'clothoid_spline_segment' in a human-readable format
inline std::ostream& operator<<(std::ostream& os, const ClothoidSplineSegment& clothoid_spline_segment)
{
  os << "ClothoidSplineSegment("
     << ".curvatureEnd=" << clothoid_spline_segment.curvature_end
     << ", .curvatureStart=" << clothoid_spline_segment.curvature_start
     << ", .hOffset=" << clothoid_spline_segment.h_offset
     << ", .length=" << clothoid_spline_segment.length;

  if (clothoid_spline_segment.position_start.has_value())
  {
    os << ", .position_start=" << clothoid_spline_segment.position_start.value();
  }
  os << ")";
  return os;
};

/// Representation of a single clothoid or a sequence of concatenated clothoids forming a spline
struct ClothoidSpline
{
  using segments_type = std::vector<ClothoidSplineSegment>;  ///< Clothoid spline segments type
  using iterator = segments_type::iterator;                  ///< Clothoid spline segments iterator
  using const_iterator = segments_type::const_iterator;      ///< Clothoid spline segments const iterator

  segments_type segments;  ///< Clothoid spline segments

  /// Return an iterator to the beginning.
  ///
  /// @returns Return an iterator to the first element of the segments
  iterator begin() { return segments.begin(); };

  /// Return an iterator to the end.
  ///
  /// @returns Return an iterator to the element next after the last element of the segments
  iterator end() { return segments.end(); };

  /// Return the const iterator to the beginning.
  ///
  /// @returns Return the const iterator to the first element of the segments
  [[nodiscard]] const_iterator cbegin() const { return segments.cbegin(); };

  /// Return the const iterator to the end.
  ///
  /// @returns Return the const iterator to the element next after the last element of the segments
  [[nodiscard]] const_iterator cend() const { return segments.cend(); };
};

/// Compare two objects of ClothoidSpline.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]  rhs right-hand side value for the comparison
/// @returns  true if the segments values of lhs exactly equals to rhs segments values
inline bool operator==(const ClothoidSpline& lhs, const ClothoidSpline& rhs) noexcept
{
  return lhs.segments == rhs.segments;
};

/// Print a human-readable representation of 'ClothoidSpline' to 'os'.
///
/// @param os             The output stream
/// @param clothoid_spline  ClothoidSpline to be printed
/// @returns 'clothoid_spline' in a human-readable format
inline std::ostream& operator<<(std::ostream& os, const ClothoidSpline& clothoid_spline)
{
  os << "ClothoidSpline(";
  for (std::size_t i = 0; i < clothoid_spline.segments.size(); i++)
  {
    os << (i == 0 ? "[" : ", [") << std::to_string(i) << "]=" << clothoid_spline.segments[i];
  }
  os << ")";
  return os;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_CLOTHOID_SPLINE_H
