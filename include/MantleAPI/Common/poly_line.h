/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file poly_line.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_POLY_LINE_H
#define MANTLEAPI_COMMON_POLY_LINE_H

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/time_utils.h>

#include <iosfwd>
#include <optional>
#include <string>
#include <tuple>
#include <vector>

namespace mantle_api
{

/// One point of a polygonal chain (polyline) trajectory specification.
struct PolyLinePoint
{
  Pose pose;                 ///< Pose of the PolyLinePoint
  std::optional<Time> time;  ///< Time specification of the PolyLinePoint
};

/// Compare the values of two PolyLinePoints.
///
/// @param[in] lhs left-hand side value for the comparison
/// @param[in] rhs right-hand side value for the comparison
/// @returns true, if the values of the two PolyLinePoints are equal, false otherwise
constexpr bool operator==(const PolyLinePoint& lhs, const PolyLinePoint& rhs) noexcept
{
  return std::tie(lhs.pose, lhs.time) == std::tie(rhs.pose, rhs.time);
}

/// Compare the values of two PolyLinePoints.
///
/// @param[in] lhs left-hand side value for the comparison
/// @param[in] rhs right-hand side value for the comparison
/// @returns true, if the values of the two PolyLinePoints are not equal, false otherwise
constexpr bool operator!=(const PolyLinePoint& lhs, const PolyLinePoint& rhs) noexcept
{
  return !(lhs == rhs);
}

/// The list of polyline points.
using PolyLine = std::vector<PolyLinePoint>;

/// Output stream operator for PolyLinePoint.
///
/// @param[in] os output stream
/// @param[in] point PolyLinePoint to be printed
/// @returns output stream with printed PolyLinePoint
///
inline std::ostream& operator<<(std::ostream& os, const PolyLinePoint& point)
{
  os << "PolyLinePoint(.pose=" << point.pose << ", .time=" << point.time.value_or(Time{}) << ')';
  return os;
}

/// Output stream operator for PolyLine.
///
/// @param[in] os output stream
/// @param[in] poly_line PolyLine to be printed
/// @returns output stream with printed PolyLine
///
inline std::ostream& operator<<(std::ostream& os, const PolyLine& poly_line)
{
  os << "PolyLine(";
  for (auto idx = 0U; idx < poly_line.size(); ++idx)
  {
    os << (idx == 0U ? "[" : ", [") << std::to_string(idx) << "]=" << poly_line.at(idx);
  }
  os << ')';
  return os;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_POLY_LINE_H
