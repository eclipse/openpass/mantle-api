/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file route_definition.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_ROUTE_DEFINITION_H
#define MANTLEAPI_COMMON_ROUTE_DEFINITION_H

#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <tuple>
#include <vector>

namespace mantle_api
{

/// Define how the route from a waypoint forward should be calculated.
enum class RouteStrategy
{
  kUndefined = 0,
  kFastest,
  kLeastIntersections,
  kShortest
};

/// Group a Waypoint with a RouteStrategy.
struct RouteWaypoint
{
  /// Reference position used to form a route
  mantle_api::Vec3<units::length::meter_t> waypoint{};
  /// Defines how a route should be calculated
  RouteStrategy route_strategy{};
};

/// Equality comparison for RouteWaypoint.
///
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs and rhs are equal
inline bool operator==(const RouteWaypoint& lhs, const RouteWaypoint& rhs) noexcept
{
  return std::tie(lhs.waypoint, lhs.route_strategy) == std::tie(rhs.waypoint, rhs.route_strategy);
}

/// Inequality comparison for RouteWaypoint.
///
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs and rhs are not equal
inline bool operator!=(const RouteWaypoint& lhs, const RouteWaypoint& rhs) noexcept
{
  return !(lhs == rhs);
}

/// A raw set of global coordinates and information for
/// linking them, from which the actual route can be calculated.
struct RouteDefinition
{
  /// The list of waypoints with associated RouteStrategies
  std::vector<mantle_api::RouteWaypoint> waypoints;
};

/// Equality comparison for RouteDefinition.
///
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs and rhs are equal
inline bool operator==(const RouteDefinition& lhs, const RouteDefinition& rhs) noexcept
{
  return lhs.waypoints == rhs.waypoints;
}

/// Inequality comparison for RouteDefinition.
///
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs and rhs are not equal
inline bool operator!=(const RouteDefinition& lhs, const RouteDefinition& rhs) noexcept
{
  return !(lhs == rhs);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_ROUTE_DEFINITION_H
