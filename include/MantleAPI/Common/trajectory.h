/*******************************************************************************
 * Copyright (c) 2021-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file trajectory.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_TRAJECTORY_H
#define MANTLEAPI_COMMON_TRAJECTORY_H

#include <MantleAPI/Common/clothoid_spline.h>
#include <MantleAPI/Common/poly_line.h>

#include <cstdint>
#include <optional>
#include <string>
#include <tuple>
#include <variant>

namespace mantle_api
{

/// Defines which reference point of the object is used for the trajectory.
enum class TrajectoryReferencePoint : std::uint8_t
{
  kUndefined = 0,     ///< Type of trajectory reference point is underfined
  kRearAxle,          ///< Reference point is the rear axle of the object
  kFrontAxle,         ///< Reference point is the front axle of the object
  kCenterOfMass,      ///< Reference point is the center of mass of the object
  kBoundingBoxCenter  ///< Reference point is the center of the bounding box of the object
};

/// Definition of a trajectory type in terms of shape
struct Trajectory
{
  std::string name;                             ///< Name of the trajectory type
  std::variant<PolyLine, ClothoidSpline> type;  ///< Trajectory type in terms of shape
  TrajectoryReferencePoint reference;           ///< Reference point of object, which is used in trajectory
};

/// Compare two objects of Trajectory.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]  rhs right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equals to rhs values
constexpr bool operator==(const Trajectory& lhs, const Trajectory& rhs) noexcept
{
  return std::tie(lhs.name, lhs.type, lhs.reference) == std::tie(rhs.name, rhs.type, rhs.reference);
}

/// Print a human-readable representation of 'trajectory' to 'os'.
///
/// @param os         The output stream
/// @param trajectory Open scenario trajectory
/// @returns 'trajectory' in a human-readable format
inline std::ostream& operator<<(std::ostream& os, const Trajectory& trajectory)
{
  os << "Trajectory(.name=" << trajectory.name << ", .type=";

  if (std::holds_alternative<PolyLine>(trajectory.type))
  {
    os << std::get<PolyLine>(trajectory.type);
  }
  else if (std::holds_alternative<ClothoidSpline>(trajectory.type))
  {
    os << std::get<ClothoidSpline>(trajectory.type);
  }

  os << ", reference=" << static_cast<int>(trajectory.reference) << ")";

  return os;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_TRAJECTORY_H
