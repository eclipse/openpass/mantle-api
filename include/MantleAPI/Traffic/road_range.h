/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file road_range.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_ROAD_RANGE_H
#define MANTLEAPI_TRAFFIC_ROAD_RANGE_H

#include <MantleAPI/Map/lane_definition.h>
#include <units.h>

#include <optional>
#include <string>
#include <vector>

namespace mantle_api
{

/// Defines a cross section on a road at a given s-coordinate
/// @note aligns with the definition within OpenSCENARIO XML v1.3.0
struct RoadCursor
{
  /// Defines a restriction of the road cursor to specific lanes of a road
  struct LaneRestriction
  {
    LaneId id;  ///< The local ID of the target lane at the specified s coordinate taken from the respective road network definition file.
  };

  std::optional<units::length::meter_t> s_offset;              ///< The s-coordinate taken along the road's reference line from the start point of the target road. If s is omitted and the road cursor depicts the start of a road range, then s=0 is assumed. If s is omitted and the road cursor depicts the end of a road range, then s=max_length is assumed. Unit: [m]. Range: [0..inf[.
  std::string road_id;                                         ///< The ID of the target road taken from the respective road network definition file.
  std::vector<RoadCursor::LaneRestriction> lane_restrictions;  ///< Restriction of the road cursor to specific lanes of a road. If omitted, road cursor is valid for all lanes of the road.
};

/// Defines an area by a range on a specific road.
/// @note aligns with the definition within OpenSCENARIO XML v1.3.0
struct RoadRange
{
  std::optional<units::length::meter_t> length;  ///< Limits the length of the road range starting from the first road cursor. If omitted or if length exceeds last road cursor, then last road cursor defines the end of the road range. Unit: [m]. Range: ]0;inf[.
  std::vector<RoadCursor> road_cursors;          ///< A minimum of 2 road cursors must be provided to specify the start and end of the road range. Intermediate cursors can be used to change the lane "validity".
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_ROAD_RANGE_H
