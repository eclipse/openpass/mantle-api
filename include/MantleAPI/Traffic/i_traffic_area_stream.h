/*******************************************************************************
 * Copyright (c) 2024-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file i_traffic_area_stream.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_I_TRAFFIC_AREA_STREAM_H
#define MANTLEAPI_TRAFFIC_I_TRAFFIC_AREA_STREAM_H

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <units.h>

#include <memory>
#include <optional>
#include <vector>

namespace mantle_api
{

/// Map agnostic representation of connected lanes
class ITrafficAreaStream
{
public:
  /// Represents a position on a stream
  struct StreamPosition
  {
    units::length::meter_t s;  ///< Longitudinal position within the stream (relative to the start)
    units::length::meter_t t;  ///< Lateral position within the stream (relative to the middle of the stream)
  };

  /// Represents a pose on a stream
  struct StreamPose : StreamPosition
  {
    units::angle::radian_t hdg;  ///< Heading relative to stream direction
  };

  /// Search direction for finding entities on the stream
  enum class SearchDirection
  {
    kUndefined = 0,
    kDownstream,
    kUpstream,
  };

  /// Gets the length of the stream
  /// @returns Length of the stream
  virtual units::length::meter_t GetLength() const = 0;

  /// Checks if a stream position lies on a lane of this stream
  /// @param stream_position stream position to check
  /// @returns True if the position is within the stream, false otherwise
  virtual bool Contains(const StreamPosition& stream_position) const = 0;

  /// Gets the first entity on the stream that overlaps with the given search interval
  /// @param search_direction Direction to search in
  /// @param start_distance   Distance from which to start the search
  /// @param search_distance  Distance to search for in the search_direction
  /// @returns Entity or empty if no entity was found
  virtual std::weak_ptr<IEntity> GetEntity(
      SearchDirection search_direction,
      units::length::meter_t start_distance,
      units::length::meter_t search_distance) const = 0;

  /// Converts a stream pose to a global pose
  /// @param stream_pose Longitudinal, lateral & angular offset along this stream's chain of lane centerlines
  /// @returns Pose if convertible, empty otherwise
  virtual std::optional<Pose> Convert(const StreamPose& stream_pose) const = 0;

  /// Converts a global pose to a pose relative to this stream's origin
  /// @param pose Global position and orientation
  /// @returns StreamPose if the given point lies on a road part of this stream, otherwise nullopt
  virtual std::optional<StreamPose> Convert(const Pose& pose) const = 0;
};

/// @brief almost-equality
/// @details Compares the values of two stream positions.
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs are almost equal to the values of rhs.
constexpr bool operator==(const ITrafficAreaStream::StreamPosition& lhs, const ITrafficAreaStream::StreamPosition& rhs)
{
  return AlmostEqual(lhs.s, rhs.s) && AlmostEqual(lhs.t, rhs.t);
}

/// @brief inequality
/// @details Compares the value of stream positions.
/// @param[in] lhs left-hand side value for the comparison
/// @param[in] rhs right-hand side value for the comparison
/// @returns true if any value of lhs is not almost equal to the value of rhs
constexpr bool operator!=(const ITrafficAreaStream::StreamPosition& lhs, const ITrafficAreaStream::StreamPosition& rhs)
{
  return !(lhs == rhs);
}

/// @brief almost-equality
/// @details Compares the values of two stream positions.
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs are almost equal to the values of rhs.
constexpr bool operator==(const ITrafficAreaStream::StreamPose& lhs, const ITrafficAreaStream::StreamPose& rhs)
{
  return static_cast<const ITrafficAreaStream::StreamPosition&>(lhs) == static_cast<const ITrafficAreaStream::StreamPosition&>(rhs) && AlmostEqual(lhs.hdg, rhs.hdg);
}

/// @brief inequality
/// @details Compares the value of stream poses.
/// @param[in] lhs left-hand side value for the comparison
/// @param[in] rhs right-hand side value for the comparison
/// @returns true if any value of lhs is not almost equal to the value of rhs
constexpr bool operator!=(const ITrafficAreaStream::StreamPose& lhs, const ITrafficAreaStream::StreamPose& rhs)
{
  return !(lhs == rhs);
}
}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_I_TRAFFIC_AREA_STREAM_H
