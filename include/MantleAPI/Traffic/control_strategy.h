/*******************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file control_strategy.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_CONTROL_STRATEGY_H
#define MANTLEAPI_TRAFFIC_CONTROL_STRATEGY_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Map/lane_definition.h>
#include <MantleAPI/Traffic/traffic_light_properties.h>
#include <MantleAPI/Traffic/vehicle_light_properties.h>

#include <utility>  // std::move
#include <vector>

namespace mantle_api
{

/// Movement domain of the control strategy, which specifies the desired movement behavior for the entity
enum class MovementDomain
{
  kUndefined = 0,
  kLateral,
  kLongitudinal,
  kBoth,
  kNone
};

/// Type of the control strategy
enum class ControlStrategyType
{
  kUndefined = 0,
  kKeepVelocity,
  kKeepLaneOffset,
  kFollowHeadingSpline,
  kFollowLateralOffsetSpline,
  kFollowVelocitySpline,
  kAcquireLaneOffset,
  kFollowTrajectory,
  kUpdateTrafficLightStates,
  kPerformLaneChange,
  kUpdateVehicleLightStates,
};

/// Defintion of all control strategies for a single entity.
/// The runtime mapping of an private action instance to the simulator core.
struct ControlStrategy
{
  /// @brief Constructs a ControlStrategy from its members
  ///
  /// @param[in] movement_domain Movement domain of the control strategy
  /// @param[in] type            Type of the control strategy
  explicit ControlStrategy(MovementDomain movement_domain = MovementDomain::kUndefined,
                           ControlStrategyType type = ControlStrategyType::kUndefined)
      : movement_domain{movement_domain}, type{type} {}

  virtual ~ControlStrategy() = default;

  // TODO: extend by bool use_dynamic_constraints when needed (false assumed at the moment)

  MovementDomain movement_domain;  ///< Movement domain of the control strategy
  ControlStrategyType type;        ///< Type of the control strategy
};

/// @brief Equality comparison for ControlStrategy.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equal to the values of rhs.
constexpr bool operator==(const ControlStrategy& lhs, const ControlStrategy& rhs) noexcept
{
  return lhs.movement_domain == rhs.movement_domain && lhs.type == rhs.type;
}

/// @brief  Inequality comparison for ControlStrategy.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the value of lhs is not almost equal to the value of rhs.
constexpr bool operator!=(const ControlStrategy& lhs, const ControlStrategy& rhs) noexcept
{
  return !(lhs == rhs);
}

/// Controls an entity to keeps current velocity
struct KeepVelocityControlStrategy : public ControlStrategy
{
  /// @brief Constructs a KeepVelocityControlStrategy as a ControlStrategy with predefined values
  KeepVelocityControlStrategy()
      : ControlStrategy{MovementDomain::kLongitudinal, ControlStrategyType::kKeepVelocity} {}
  // Doesn't need configuration attributes. Controller keeps current velocity on adding entity or update
};

/// Controls an entity to keeps current lane offset
struct KeepLaneOffsetControlStrategy : public ControlStrategy
{
  /// @brief Constructs a KeepLaneOffsetControlStrategy as a ControlStrategy with predefined values
  KeepLaneOffsetControlStrategy()
      : ControlStrategy{MovementDomain::kLateral, ControlStrategyType::kKeepLaneOffset} {}
  // Doesn't need configuration attributes. Controller keeps current lane offset on adding entity or update
};

/// Controls an entity to follow a heading angle from spline
struct FollowHeadingSplineControlStrategy : public ControlStrategy
{
  /// @brief Constructs a FollowHeadingSplineControlStrategy
  /// as a ControlStrategy with predefined values
  ///
  /// @param[in] heading_splines List of the heading angles of splines
  /// @param[in] default_value   The default value of the heading angle
  explicit FollowHeadingSplineControlStrategy(
      std::vector<mantle_api::SplineSection<units::angle::radian>> heading_splines = {},
      units::angle::radian_t default_value = {})
      : ControlStrategy{MovementDomain::kLateral, ControlStrategyType::kFollowHeadingSpline},
        heading_splines{std::move(heading_splines)},
        default_value{default_value} {}

  /// List of the heading angles of splines
  std::vector<mantle_api::SplineSection<units::angle::radian>> heading_splines;
  /// The default value of the heading angle
  units::angle::radian_t default_value;
};

/// Controls an entity to follow a velocity from spline
struct FollowVelocitySplineControlStrategy : public ControlStrategy
{
  /// @brief Constructs a FollowVelocitySplineControlStrategy
  /// as a ControlStrategy with predefined values
  ///
  /// @param[in] velocity_splines List of the velocities of splines
  /// @param[in] default_value    The default value of the velocity
  explicit FollowVelocitySplineControlStrategy(
      std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines = {},
      units::velocity::meters_per_second_t default_value = {})
      : ControlStrategy{MovementDomain::kLongitudinal, ControlStrategyType::kFollowVelocitySpline},
        velocity_splines{std::move(velocity_splines)},
        default_value{default_value} {}

  /// List of the velocities of splines
  std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines;
  /// The default value of the velocity
  units::velocity::meters_per_second_t default_value;
};

/// @brief Equality comparison for FollowVelocitySplineControlStrategy.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equal to the values of rhs.
constexpr bool operator==(const FollowVelocitySplineControlStrategy& lhs,
                          const FollowVelocitySplineControlStrategy& rhs) noexcept
{
  return lhs.default_value == rhs.default_value && lhs.velocity_splines == rhs.velocity_splines;
}

/// @brief  Inequality comparison for FollowVelocitySplineControlStrategy.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the value of lhs is not almost equal to the value of rhs.
constexpr bool operator!=(const FollowVelocitySplineControlStrategy& lhs,
                          const FollowVelocitySplineControlStrategy& rhs) noexcept
{
  return !(lhs == rhs);
}

/// Controls an entity to follow a lateral offset from spline
struct FollowLateralOffsetSplineControlStrategy : public ControlStrategy
{
  /// @brief Constructs a FollowLateralOffsetSplineControlStrategy
  /// as a ControlStrategy with predefined values
  ///
  /// @param[in] lateral_offset_splines List of the lateral offsets of splines
  explicit FollowLateralOffsetSplineControlStrategy(
      std::vector<mantle_api::SplineSection<units::length::meter>> lateral_offset_splines = {})
      : ControlStrategy{MovementDomain::kLateral, ControlStrategyType::kFollowLateralOffsetSpline},
        lateral_offset_splines{std::move(lateral_offset_splines)} {}

  /// List of the lateral offsets of splines
  std::vector<mantle_api::SplineSection<units::length::meter>> lateral_offset_splines;
};

/// Defines how a target value will be acquired (with a constant rate, in a defined distance, within a defined time).
enum class Dimension
{
  kUndefined = 0,
  kDistance,
  kRate,
  kTime
};

/// Function type used to represent the change of a given variable over time or distance.
enum class Shape
{
  kUndefined = 0,
  kStep,
  kCubic,
  kLinear,
  kSinusoidal
};

/// Specifies the dynamics of a value transition and defines how the value changes over time or distance.
struct TransitionDynamics
{
  /// Defines how a target value will be acquired
  Dimension dimension{Dimension::kUndefined};
  /// The shape of the transition function between current and target value
  Shape shape{Shape::kUndefined};
  /// The value for a predefined rate, time or distance to acquire the target value
  double value{0.0};
};

/// Controls the transition to a defined lane offset of an entity
struct AcquireLaneOffsetControlStrategy : public ControlStrategy
{
  /// @brief Constructs a AcquireLaneOffsetControlStrategy
  /// as a ControlStrategy with predefined values
  ///
  /// @param[in] offset               Lane offset
  /// @param[in] transition_dynamics  Specifies the dynamics of a value transition
  explicit AcquireLaneOffsetControlStrategy(units::length::meter_t offset = {},
                                            TransitionDynamics transition_dynamics = {})
      : ControlStrategy{MovementDomain::kLateral, ControlStrategyType::kAcquireLaneOffset},
        offset{offset},
        transition_dynamics{transition_dynamics} {}

  /// Lane offset
  units::length::meter_t offset;
  /// Specifies the dynamics of a value transition
  TransitionDynamics transition_dynamics;
};

/// Controls the transition of a vehicle light state to the target light state
struct VehicleLightStatesControlStrategy : public ControlStrategy
{
  VehicleLightStatesControlStrategy()
  {
    type = ControlStrategyType::kUpdateVehicleLightStates;
    movement_domain = MovementDomain::kNone;
  }

  LightType light_type;    ///< Light type
  LightState light_state;  ///< Light state
};

/// Controls the transition of a current light state to the target light state
struct TrafficLightStateControlStrategy : public ControlStrategy
{
  /// @brief Constructs a TrafficLightStateControlStrategy
  /// as a ControlStrategy with predefined values
  ///
  /// @param[in] traffic_light_phases List of specific traffic light phases
  /// @param[in] repeat_states        Whether the light state is repeated or not
  explicit TrafficLightStateControlStrategy(std::vector<TrafficLightPhase> traffic_light_phases = {},
                                            bool repeat_states = false)
      : ControlStrategy{MovementDomain::kNone, ControlStrategyType::kUpdateTrafficLightStates},
        traffic_light_phases{std::move(traffic_light_phases)},
        repeat_states{repeat_states} {}

  /// List of specific traffic light phases
  std::vector<TrafficLightPhase> traffic_light_phases;
  /// The "repeat_states" flag determines, if the light state is repeated or not
  bool repeat_states;
};

/// Definition of time value context as either absolute or relative
enum class ReferenceContext
{
  kAbsolute = 0,
  kRelative
};

/// Controls an entity to follow a trajectory
struct FollowTrajectoryControlStrategy : public ControlStrategy
{
  // TODO: Extend the FollowTrajectoryControlStrategy to support shapes like NURBS and clothoid

  /// Definition of the time information present in trajectory
  struct TrajectoryTimeReference
  {
    /// Specification of the time domain (absolute or relative)
    ReferenceContext domainAbsoluteRelative{ReferenceContext::kAbsolute};
    /// Scaling factor for time values.
    /// While values smaller than 1.0 represent negative scaling, values larger than 1.0 will result in positive scaling.
    /// A value of 1.0 means no scaling.
    double scale{1.0};
    /// Global offset for all time values
    units::time::second_t offset{0.0};
  };

  /// @brief Constructs a FollowTrajectoryControlStrategy as a ControlStrategy with predefined values
  ///
  /// @param[in] trajectory    Chain of points describing a trajectory
  /// @param[in] timeReference Time information provided within the trajectory
  explicit FollowTrajectoryControlStrategy(Trajectory trajectory = {},
                                           std::optional<TrajectoryTimeReference> timeReference = std::nullopt)
      : ControlStrategy{MovementDomain::kBoth, ControlStrategyType::kFollowTrajectory},
        trajectory{std::move(trajectory)},
        timeReference{timeReference} {}

  /// Trajectory definition
  Trajectory trajectory;
  /// Time information provided within the trajectory
  std::optional<TrajectoryTimeReference> timeReference;
};

/// Controls an entity to perform a lane change.
/// Describes the transition between an entity's current lane and its target lane.
struct PerformLaneChangeControlStrategy : public ControlStrategy
{
  /// @brief Constructs a PerformLaneChangeControlStrategy as a ControlStrategy with predefined values
  ///
  /// @param[in] target_lane_id ID of the target lane the entity will change to
  /// @param[in] target_lane_offset Lane offset to be reached at the target lane
  /// @param[in] transition_dynamics Shape/time of lane change action
  explicit PerformLaneChangeControlStrategy(mantle_api::LaneId target_lane_id = {},
                                            units::length::meter_t target_lane_offset = {},
                                            TransitionDynamics transition_dynamics = {})
      : ControlStrategy{MovementDomain::kLateral, ControlStrategyType::kPerformLaneChange},
        target_lane_id{target_lane_id},
        target_lane_offset{target_lane_offset},
        transition_dynamics{transition_dynamics} {}

  /// ID of the target lane the entity will change to
  mantle_api::LaneId target_lane_id;
  /// Lane offset to be reached at the target lane (the action will end there)
  units::length::meter_t target_lane_offset;
  /// Shape/time of lane change action
  TransitionDynamics transition_dynamics;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_CONTROL_STRATEGY_H
