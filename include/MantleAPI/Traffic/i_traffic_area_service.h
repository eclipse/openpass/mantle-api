/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file i_traffic_area_service.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_I_TRAFFIC_AREA_SERVICE_H
#define MANTLEAPI_TRAFFIC_I_TRAFFIC_AREA_SERVICE_H

#include <MantleAPI/Traffic/i_traffic_area_stream.h>
#include <MantleAPI/Traffic/road_range.h>

#include <memory>
#include <vector>

namespace mantle_api
{

/// Collection of traffic area streams belonging to a RoadRange.
using TrafficArea = std::vector<std::shared_ptr<ITrafficAreaStream>>;

/// Abstraction layer for managing traffic area related queries
class ITrafficAreaService
{
public:
  virtual ~ITrafficAreaService() = default;

  /// Creates a traffic area object
  /// @note Ownership of the returned object is shared with the caller
  /// @param road_range Road range defining the traffic area
  /// @returns Collection of traffic area streams
  virtual TrafficArea CreateTrafficArea(const RoadRange& road_range) const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_I_TRAFFIC_AREA_SERVICE_H
