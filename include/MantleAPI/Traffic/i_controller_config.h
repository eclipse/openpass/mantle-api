/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file i_controller_config.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_I_CONTROLLER_CONFIG_H
#define MANTLEAPI_TRAFFIC_I_CONTROLLER_CONFIG_H

#include <MantleAPI/Common/route_definition.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Map/i_lane_location_query_service.h>
#include <MantleAPI/Traffic/control_strategy.h>

#include <map>
#include <memory>
#include <vector>

namespace mantle_api
{

/// Base definition for creating a new controller for an entity
struct IControllerConfig
{
  /// @brief Constructs a IControllerConfig from its members
  ///
  /// @param[in] name               Name of the controller. Might be ignored by the environment.
  /// @param[in] map_query_service  Pointer to the map query service
  /// @param[in] control_strategies List of active control strategies
  /// @param[in] route_definition   Specifies the route behavior for the control stratgies
  explicit IControllerConfig(std::string name = {},
                             ILaneLocationQueryService* map_query_service = nullptr,
                             std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies = {},
                             RouteDefinition route_definition = {})
      : name{std::move(name)},
        map_query_service{map_query_service},
        control_strategies{std::move(control_strategies)},
        route_definition{std::move(route_definition)} {}

  /// @brief default destructor
  virtual ~IControllerConfig() = default;
  /// @brief Name of the controller. Might be ignored by the environment.
  std::string name;
  /// Pointer to the map query service
  /// @todo: Check why map_query_service is part of the interface because it is not set from engine side but only in the environment on calling AddController()
  ILaneLocationQueryService* map_query_service;
  /// List of active control strategies
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
  /// Specifies the route behavior for the control stratgies
  RouteDefinition route_definition;
};

/// @brief Equality comparison for IControllerConfig.
///
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs and rhs are equal
inline bool operator==(const IControllerConfig& lhs, const IControllerConfig& rhs) noexcept
{
  // Check if the objects are the same
  if (&lhs == &rhs)
  {
    return true;
  }

  const auto compare_control_strategy = [](const auto& lhs, const auto& rhs)
  {
    // Compare the objects pointed to by the shared_ptrs, handling the case where either is null
    if (!lhs || !rhs)
    {
      return lhs == rhs;  // Both are null (equal) or one is null (not equal)
    }
    return *lhs == *rhs;  // Both are non-null, compare the objects
  };

  return lhs.name == rhs.name &&
         lhs.map_query_service == rhs.map_query_service &&
         std::equal(lhs.control_strategies.begin(), lhs.control_strategies.end(), rhs.control_strategies.begin(), rhs.control_strategies.end(), compare_control_strategy) &&
         lhs.route_definition == rhs.route_definition;
}

/// @brief Check for not equal
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs and rhs are not equal
inline bool operator!=(const IControllerConfig& lhs, const IControllerConfig& rhs) noexcept
{
  return !(lhs == rhs);
}

/// Define a "No Operation" controller
struct NoOpControllerConfig : public IControllerConfig
{
};

/// Define a default or internal controller (usually one per entity)
struct InternalControllerConfig : public IControllerConfig
{
};

/// Define an external controller with custom parameters
struct ExternalControllerConfig : public IControllerConfig
{
  /// Additional parameters as name value pair
  std::map<std::string, std::string> parameters;
};

/// @brief Check for equality
///
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs and rhs are equal
inline bool operator==(const ExternalControllerConfig& lhs, const ExternalControllerConfig& rhs) noexcept
{
  return static_cast<const IControllerConfig&>(lhs) == static_cast<const IControllerConfig&>(rhs) &&
         lhs.parameters == rhs.parameters;
}

/// @brief Check for not equal
///
/// @param[in] lhs The left-hand side value for the comparison
/// @param[in] rhs The right-hand side value for the comparison
/// @returns true if the values of lhs and rhs are not equal
inline bool operator!=(const ExternalControllerConfig& lhs, const ExternalControllerConfig& rhs) noexcept
{
  return !(lhs == rhs);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_I_CONTROLLER_CONFIG_H
