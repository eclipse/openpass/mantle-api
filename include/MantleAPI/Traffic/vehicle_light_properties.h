/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/// @file vehicle_light_properties.h
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_VEHICLE_LIGHT_PROPERTIES_H
#define MANTLEAPI_TRAFFIC_VEHICLE_LIGHT_PROPERTIES_H

#include <string>
#include <variant>

namespace mantle_api
{

/// Defines the vehicle light type
enum class VehicleLightType
{
  kUndefined = 0,
  kBrakeLights,
  kDaytimeRunningLights,
  kFogLights,
  kFogLightsFront,
  kFogLightsRear,
  kHighBeam,
  kIndicatorLeft,
  kIndicatorRight,
  kLicensePlateIllumination,
  kLowBeam,
  kReversingLights,
  kSpecialPurposeLights,
  kWarningLights
};

/// Light type variant of vehicle light type or user defined light
using LightType = std::variant<VehicleLightType, std::string>;

/// Defines the light mode
enum class LightMode
{
  kUndefined = 0,
  kFlashing,
  kOff,
  kOn
};

/// Specifies the light state
struct LightState
{
  LightMode light_mode;  ///< Light mode
};

/// Equality comparison for LightState.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equal to the values of rhs.
constexpr bool operator==(const LightState& lhs,
                          const LightState& rhs) noexcept
{
  return lhs.light_mode == rhs.light_mode;
}

/// Inequality comparison for LightState.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the value of lhs is not equal to the value of rhs.
constexpr bool operator!=(const LightState& lhs,
                          const LightState& rhs) noexcept
{
  return !(lhs == rhs);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_VEHICLE_LIGHT_PROPERTIES_H
