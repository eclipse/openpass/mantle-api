#!/bin/bash

################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script checks the inline documentation using doxygen
################################################################################

# compare two versions using semantic version syntax
#
# usage: greater_or_equal "1.2.3" "2.3.4"
# param lhs: left hand side of the operation "lhs >= rhs"
# param rhs: right hand side of the operation "lhs >= rhs"
greater_or_equal()
{
    local v_lhs="$1"
    local v_rhs="$2"
    local version_regex='([0-9]+\.[0-9]+\.[0-9]+)'

    [[ $v_lhs =~ $version_regex ]] && v_lhs=${BASH_REMATCH[1]}
    [[ $v_rhs =~ $version_regex ]] && v_rhs=${BASH_REMATCH[1]}

    IFS='.' read -r -a i_lhs <<< "$v_lhs"
    IFS='.' read -r -a i_rhs <<< "$v_rhs"

    for ((i = 0; i < ${#i_rhs[@]}; i++)); do
        if [ "${i_rhs[i]}" -gt "${i_lhs[i]}" ]; then
            return 1
        fi
    done
    return 0
}

sed_if_greater_or_equal()
{
    local lhs="$1"
    local rhs="$2"
    local message="$3"
    local sed_command="$4"

    if greater_or_equal "$lhs" "$rhs"; then
        echo "Filtering warning/errors for doxygen "$lhs": $message"
        echo "sed -i \"$sed_command\" \"doc/DoxygenWarningLog.txt\""
        sed -i "$sed_command" "doc/DoxygenWarningLog.txt"
    fi
}

filter()
{
    sed_if_greater_or_equal $doxy_version "${1}" "${2}" "${3}"
}

MYDIR="$(dirname "$(readlink -f "$0")")"
cd "$MYDIR/../../../../build" || exit 1

doxy_version="$(doxygen --version | grep -oP '[0-9]+\.[0-9]+\.[0-9]+')"
echo "Doxygen Version $doxy_version"
make MantleAPI_doc

# filter diverse erroneous messages due to bugs in the individual doxygen versions

# bug description - https://github.com/doxygen/doxygen/issues/7411
# fixed in 1.8.18 - https://github.com/doxygen/doxygen/pull/7483
filter "1.8.17" \
       "Warnings \"return type of member ... is not documented\" (see https://github.com/doxygen/doxygen/issues/7411)" \
       "/warning: return type of member/d"

# issues with markdown importer
filter "1.9.1" \
       "Warning: README\.md##: warning: Unsupported xml/html tag <TAG> found" \
       "/README.md:[0-9]\+: warning: Unsupported xml\/html tag <.*> found/d"

# issues with quoted text
filter "1.9.1" \
       "Warning: floating_point_helper: Exlicit link request to 'epsilon()' could not be resolved" \
       "/warning: explicit link request to 'epsilon()' could not be resolved/d"

# bug description - https://github.com/doxygen/doxygen/issues/8091
filter "1.9.7" \
       "Warnings \"Member MantleAPI_ ... is not documented\" (see https://github.com/doxygen/doxygen/issues/8091)" \
       "/warning: Member MantleAPI_.*is not documented/d"
filter "1.9.7" \
       "Error: Problems running dot: exit code=2, command='dot'" \
       "/error: Problems running dot: exit code=2, command='dot'/d"
filter "1.9.7" \
       "Error: Problems running dot: exit code=2, command='dot.exe'" \
       "/error: Problems running dot: exit code=2, command='dot.exe'/d"

# filtering warnings not related to in-line documentation
sed -i "/Detected potential recursive class relation/d" "doc/DoxygenWarningLog.txt"

# hotfixing CI (see issue https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/issues/68)
sed -i "/error: Problems running gswin32c.exe. Check your installation\!/d" "doc/DoxygenWarningLog.txt"

# remove blank lines
sed -i '/^\s*$/d' "doc/DoxygenWarningLog.txt"

if [ -s "doc/DoxygenWarningLog.txt" ]; then
     echo "ERROR: Doxygen warnings"
     cat "doc/DoxygenWarningLog.txt"
     exit 1
else
     echo "No Doxygen warnings found"
     exit 0
fi
