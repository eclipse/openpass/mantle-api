################################################################################
# Copyright (c) 2024 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

find_file(PLANTUML_JAR plantuml.jar PATHS ${PLANTUML_JAR_PATH} ENV{PLANTUML_JAR_PATH} /usr/share/plantuml)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PlantUML DEFAULT_MSG PLANTUML_JAR)

if(PlantUML_FOUND)
  if(NOT TARGET PlantUML::PlantUML)
    add_executable(PlantUML::PlantUML IMPORTED)
    if(EXISTS "${PLANTUML_JAR}")
      set_target_properties(PlantUML::PlantUML PROPERTIES IMPORTED_LOCATION "${PLANTUML_JAR}")
    endif()
  endif()
endif()
