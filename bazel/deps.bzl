load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

ECLIPSE_GITLAB = "https://gitlab.eclipse.org/eclipse"

UNITS_TAG = "2.3.4"

def mantle_api_deps():
    """Load dependencies"""
    maybe(
        http_archive,
        name = "units_nhh",
        url = "https://github.com/nholthaus/units/archive/refs/tags/v{tag}.tar.gz".format(tag = UNITS_TAG),
        sha256 = "e7c7d307408c30bfd30c094beea8d399907ffaf9ac4b08f4045c890f2e076049",
        strip_prefix = "./units-{tag}".format(tag = UNITS_TAG),
        build_file = "//bazel:units.BUILD",
    )
