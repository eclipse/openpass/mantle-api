/*******************************************************************************
 * Copyright (c) 2024, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "MantleAPI/Execution/i_environment_engine.h"
#include "MantleAPI/Execution/mock_environment_engine.h"

namespace
{

using testing::_;
using testing::Const;

class EnvironmentEngineTest : public testing::Test
{
protected:
  mantle_api::MockEnvironmentEngine mock_environment_engine_;
  mantle_api::IEnvironmentEngine &environment_engine_{mock_environment_engine_};
};

TEST_F(EnvironmentEngineTest, GetEnvironment)
{
  EXPECT_CALL(mock_environment_engine_, GetEnvironment()).Times(1);
  ASSERT_FALSE(environment_engine_.GetEnvironment());
}

TEST_F(EnvironmentEngineTest, Step)
{
  EXPECT_CALL(mock_environment_engine_, Step(_)).Times(1);
  ASSERT_NO_THROW(environment_engine_.Step({}));
}

TEST_F(EnvironmentEngineTest, GetDesiredDeltaTime)
{
  EXPECT_CALL(Const(mock_environment_engine_), GetDesiredDeltaTime()).Times(1);
  ASSERT_FALSE(environment_engine_.GetDesiredDeltaTime());
}

}  // namespace
