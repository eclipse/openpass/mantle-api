/*******************************************************************************
 * Copyright (c) 2024, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MANTLEAPI_COMMON_MOCK_ENVIRONMENT_ENGINE_H
#define MANTLEAPI_COMMON_MOCK_ENVIRONMENT_ENGINE_H

#include <gmock/gmock.h>

#include "MantleAPI/Execution/i_environment_engine.h"

namespace mantle_api
{

class MockEnvironmentEngine final : public IEnvironmentEngine
{
public:
  MOCK_METHOD(IEnvironment*, GetEnvironment, (), (noexcept, override));
  MOCK_METHOD(void, Step, (Time size), (override));
  MOCK_METHOD(std::optional<Time>, GetDesiredDeltaTime, (), (const, noexcept, override));
};

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_MOCK_ENVIRONMENT_ENGINE_H
