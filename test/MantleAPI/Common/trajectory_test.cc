/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <MantleAPI/Common/trajectory.h>
#include <gtest/gtest.h>

#include <functional>
#include <iostream>
#include <sstream>
#include <variant>

using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_curv;
using units::literals::operator""_s;

using mantle_api::ClothoidSpline;
using mantle_api::PolyLine;
using mantle_api::Trajectory;

namespace
{
class TrajectoryTest : public ::testing::Test
{
public:
  TrajectoryTest()
  {
    clothoid_spline_.segments.push_back({0.0_curv,
                                         6.6_curv,
                                         0.4_rad,
                                         1.0_m,
                                         std::nullopt});

    clothoid_spline_.segments.push_back({
        7.0_curv,
        -7.6_curv,
        0.5_rad,
        0.2_m,
        mantle_api::Pose{{96.8_m, -4.9_m, 1.6_m}, {-0.3_rad, 2.8_rad, -2.0_rad}},
    });

    polyline_.push_back({{{1.0_m, 51.0_m, 8.3_m}, {0.8_rad, 0.5_rad, 0.4_rad}}, 0.5_s});
    polyline_.push_back({{{1.0_m, 51.0_m, 8.3_m}, {-0.8_rad, 0.5_rad, 0.4_rad}}, 0.6_s});

    trajectory_left_.name = "Trajectory";
    trajectory_left_.reference = mantle_api::TrajectoryReferencePoint::kCenterOfMass;
  }

protected:
  ClothoidSpline clothoid_spline_;
  PolyLine polyline_;
  Trajectory trajectory_left_;
  Trajectory trajectory_right_;
};

TEST_F(TrajectoryTest, GivenTrajectories_WhenTrajectoriesHaveEqualParameters_ThenTrajectoryEqualOperatorReturnsTrue)
{
  trajectory_left_.type = clothoid_spline_;
  trajectory_right_ = trajectory_left_;
  EXPECT_TRUE(trajectory_left_ == trajectory_right_);

  trajectory_left_.type = polyline_;
  trajectory_right_ = trajectory_left_;
  EXPECT_TRUE(trajectory_left_ == trajectory_right_);
}

using ChangeParam = std::function<void(Trajectory&)>;

void ChangeName(Trajectory& trajectory)
{
  trajectory.name = "New name";
}

void ChangeType(Trajectory& trajectory)
{
  if (std::holds_alternative<PolyLine>(trajectory.type))
  {
    auto& polyline = std::get<PolyLine>(trajectory.type);
    polyline.front().pose.position.x = 100_m;
  }
  else if (std::holds_alternative<ClothoidSpline>(trajectory.type))
  {
    auto& clothoid = std::get<ClothoidSpline>(trajectory.type);
    clothoid.segments.front().curvature_end = 100.0_curv;
  }
}

void ChangeReference(Trajectory& trajectory)
{
  trajectory.reference = mantle_api::TrajectoryReferencePoint::kRearAxle;
}

class TrajectoryTestParam : public ::testing::WithParamInterface<ChangeParam>, public TrajectoryTest
{
};

INSTANTIATE_TEST_SUITE_P(TrajectoryTestInstParam,
                         TrajectoryTestParam,
                         ::testing::Values(
                             ChangeName,
                             ChangeType,
                             ChangeReference));

TEST_P(TrajectoryTestParam, GivenTrajectories_WhenTrajectoriesHaveDifferentParameters_ThenTrajectoryEqualOperatorReturnsFalse)
{
  trajectory_left_.type = polyline_;
  trajectory_right_ = trajectory_left_;
  GetParam()(trajectory_right_);
  EXPECT_FALSE(trajectory_left_ == trajectory_right_);

  trajectory_left_.type = clothoid_spline_;
  trajectory_right_ = trajectory_left_;
  GetParam()(trajectory_right_);
  EXPECT_FALSE(trajectory_left_ == trajectory_right_);
}

TEST_F(TrajectoryTest, GivenTrajectoryWithNoType_WhenOutputToStream_ThenNoExceptionAndOutputNotEmpty)
{
  std::stringstream actual_os;
  auto output_to_stream = [](std::ostream& os, Trajectory& trajectory)
  {
    os << trajectory;
  };
  ASSERT_NO_THROW(output_to_stream(actual_os, trajectory_left_));
  EXPECT_GT(actual_os.str().size(), 0);
}

TEST_F(TrajectoryTest, GivenTrajectoryWithPolylineType_WhenOutputToStream_ThenNoExceptionAndOutputNotEmpty)
{
  std::stringstream actual_os;
  trajectory_left_.type = polyline_;
  auto output_to_stream = [](std::ostream& os, Trajectory& trajectory)
  {
    os << trajectory;
  };
  ASSERT_NO_THROW(output_to_stream(actual_os, trajectory_left_));
  EXPECT_GT(actual_os.str().size(), 0);
}

TEST_F(TrajectoryTest, GivenTrajectoryWithClothoidSplineType_WhenOutputToStream_ThenNoExceptionAndOutputNotEmpty)
{
  std::stringstream actual_os;
  trajectory_left_.type = clothoid_spline_;
  auto output_to_stream = [](std::ostream& os, Trajectory& trajectory)
  {
    os << trajectory;
  };
  ASSERT_NO_THROW(output_to_stream(actual_os, trajectory_left_));
  EXPECT_GT(actual_os.str().size(), 0);
}
}  // namespace
