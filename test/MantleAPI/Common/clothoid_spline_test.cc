/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <MantleAPI/Common/clothoid_spline.h>
#include <MantleAPI/Common/pose.h>
#include <gtest/gtest.h>

#include <functional>
#include <iostream>
#include <optional>
#include <sstream>

using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_curv;

using mantle_api::ClothoidSpline;
using mantle_api::ClothoidSplineSegment;

namespace
{
class ClothoidSplineTest : public ::testing::Test
{
public:
  ClothoidSplineTest()
  {
    clothoid_spline_left_.segments.push_back({0.0_curv,
                                              6.6_curv,
                                              0.4_rad,
                                              1.0_m,
                                              std::nullopt});

    clothoid_spline_left_.segments.push_back(
        {
            7.0_curv,
            -7.6_curv,
            0.5_rad,
            0.2_m,
            mantle_api::Pose{{96.8_m, -4.9_m, 1.6_m}, {-0.3_rad, 2.8_rad, -2.0_rad}},
        });
    clothoid_spline_right_ = clothoid_spline_left_;
  }

protected:
  ClothoidSpline clothoid_spline_left_;
  ClothoidSpline clothoid_spline_right_;
};

TEST_F(ClothoidSplineTest, GivenClothoidSplineSegments_WhenSegmentsHaveEqualParameters_ThenClothoidSplineSegmentEqualOperatorReturnsTrue)
{
  ClothoidSplineSegment segment_left = clothoid_spline_left_.segments.back();
  ClothoidSplineSegment segment_rigt = clothoid_spline_left_.segments.back();

  EXPECT_TRUE(segment_left == segment_rigt);
}

using ChangeParam = std::function<void(ClothoidSplineSegment&)>;

void ChangeCurvatureEnd(ClothoidSplineSegment& segment)
{
  segment.curvature_end = 6.7_curv;
}

void ChangeCurvatureStart(ClothoidSplineSegment& segment)
{
  segment.curvature_start = 5.7_curv;
}

void ChangeHOffset(ClothoidSplineSegment& segment)
{
  segment.h_offset = 188.0_rad;
}

void ChangeLength(ClothoidSplineSegment& segment)
{
  segment.length = 256.0_m;
}

void ChangePositionStart(ClothoidSplineSegment& segment)
{
  if (segment.position_start.has_value())
  {
    segment.position_start.value().position.x = 6.8_m;
  }
}

class ClothoidSplineTestParam : public ::testing::WithParamInterface<ChangeParam>, public ClothoidSplineTest
{
};

INSTANTIATE_TEST_SUITE_P(ClothoidSplineTestInstParam,
                         ClothoidSplineTestParam,
                         ::testing::Values(
                             ChangeCurvatureEnd,
                             ChangeCurvatureStart,
                             ChangeHOffset,
                             ChangeLength,
                             ChangePositionStart));

TEST_P(ClothoidSplineTestParam, GivenClothoidSplineSegments_WhenSegmentsHaveDifferentParameters_ThenClothoidSplineSegmentEqualOperatorReturnsFalse)
{
  ClothoidSplineSegment segment_left = clothoid_spline_left_.segments.back();
  ClothoidSplineSegment segment_right = clothoid_spline_left_.segments.back();

  GetParam()(segment_right);

  EXPECT_FALSE(segment_left == segment_right);
}

TEST_F(ClothoidSplineTest, GivenClothoidSplines_WhenSegmentsHaveEqualParameters_ThenClothoidSplineEqualOperatorReturnsTrue)
{
  EXPECT_TRUE(clothoid_spline_left_ == clothoid_spline_right_);
}

TEST_F(ClothoidSplineTest, GivenClothoidSplines_WhenSegmentsHaveDifferentParameters_ThenClothoidSplineEqualOperatorReturnsFalse)
{
  clothoid_spline_right_.segments.back().curvature_end = 6.7_curv;
  EXPECT_FALSE(clothoid_spline_left_ == clothoid_spline_right_);
}

TEST_F(ClothoidSplineTest, GivenClothoidSplines_WhenSegmentsHaveDifferentSize_ThenClothoidSplineEqualOperatorReturnsFalse)
{
  clothoid_spline_left_.segments.push_back(ClothoidSplineSegment());
  EXPECT_FALSE(clothoid_spline_left_ == clothoid_spline_right_);
}

TEST_F(ClothoidSplineTest, GivenClothoidSpline_WhenOutputToStream_ThenNoExceptionAndOutputNotEmpty)
{
  auto output_to_stream = [](std::ostream& os, ClothoidSpline& clothoid_spline)
  {
    os << clothoid_spline;
  };
  std::ostringstream actual_os;
  ASSERT_NO_THROW(output_to_stream(actual_os, clothoid_spline_left_));
  EXPECT_GT(actual_os.str().size(), 0);
}

TEST_F(ClothoidSplineTest, GivenClothoidSpline_WhenIterate_ThenIterationIsCorrect)
{
  auto actual_it{clothoid_spline_left_.begin()};
  auto expected_it{clothoid_spline_left_.segments.begin()};

  EXPECT_EQ(clothoid_spline_left_.begin(), clothoid_spline_left_.segments.begin());
  EXPECT_EQ(clothoid_spline_left_.end(), clothoid_spline_left_.segments.end());

  while (actual_it != clothoid_spline_left_.end())
  {
    // Test if increment is correct
    EXPECT_EQ(actual_it, expected_it);

    // Test if dereferencing is correct
    EXPECT_EQ(*actual_it, *expected_it);

    ++actual_it;
    ++expected_it;
  }

  EXPECT_EQ(actual_it, clothoid_spline_left_.segments.end());
}

TEST_F(ClothoidSplineTest, GivenConstClothoidSpline_WhenIterate_ThenIterationIsCorrect)
{
  const ClothoidSpline clothoid_spline{clothoid_spline_left_};
  auto actual_it{clothoid_spline.cbegin()};
  auto expected_it{clothoid_spline.segments.cbegin()};

  EXPECT_EQ(clothoid_spline.cbegin(), clothoid_spline.segments.cbegin());
  EXPECT_EQ(clothoid_spline.cend(), clothoid_spline.segments.cend());

  while (actual_it != clothoid_spline.cend())
  {
    // Test if increment is correct
    EXPECT_EQ(actual_it, expected_it);

    // Test if dereferencing is correct
    EXPECT_EQ(*actual_it, *expected_it);

    ++actual_it;
    ++expected_it;
  }

  EXPECT_EQ(actual_it, clothoid_spline.segments.cend());
}
}  // namespace
