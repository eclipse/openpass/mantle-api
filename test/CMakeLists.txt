################################################################################
# Copyright (c) 2022 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

CPMAddPackage(
  NAME googletest
  GITHUB_REPOSITORY google/googletest
  VERSION 1.15.2
  OPTIONS "INSTALL_GTEST ${MantleAPI_INSTALL_MOCKS}" "gtest_force_shared_crt ON"
)

# Generate a translation unit that includes all interface headers
list(TRANSFORM INTERFACE_HEADER_SET PREPEND "#include \"")
list(TRANSFORM INTERFACE_HEADER_SET APPEND "\"\n")
file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/verify_interface_header_set.cpp" ${INTERFACE_HEADER_SET})

add_executable(MantleAPITest)

target_sources(MantleAPITest PUBLIC interface_test.cpp "${CMAKE_CURRENT_BINARY_DIR}/verify_interface_header_set.cpp")

target_include_directories(MantleAPITest PRIVATE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/test>)

target_link_libraries(MantleAPITest PUBLIC MantleAPI::MantleAPI GTest::gmock_main)

include(GoogleTest)
gtest_discover_tests(MantleAPITest)

add_subdirectory(MantleAPI)

if(MantleAPI_INSTALL_MOCKS)
  add_library(Mocks INTERFACE)
  add_library(MantleAPI::Mocks ALIAS Mocks)

  target_link_libraries(Mocks INTERFACE MantleAPI::MantleAPI GTest::gmock)

  target_include_directories(
    Mocks INTERFACE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/test> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
  )

  target_compile_features(Mocks INTERFACE cxx_std_17)

  install(
    DIRECTORY MantleAPI
    TYPE INCLUDE
    FILES_MATCHING
    PATTERN "mock_*.h"
  )
  install(TARGETS Mocks EXPORT MantleAPITargets)
endif()
